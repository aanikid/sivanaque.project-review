const inputEl = document.querySelector(".value-task")
const submitEl = document.querySelector(".submit-btn")

/**
 * @description : Fonction qui permet d'ajouter un son d'alerte
 * @param inputValue
 * @param onSuccess
 * @returns {Promise<void>}
 */
const audioAlert = async (inputValue, onSuccess) => {
    if (inputValue === "") {
        await (new Audio("assets/sound/error.wav")).play()
        alert("Ow noo..!\nYou have to enter something about your task. It would be kind of embarrassing to remember after...");
    }else{
        await (new Audio("assets/sound/success.wav")).play()
        onSuccess();
    }
};

/**
 * @description : Fonction qui permet d'ajouter un élément dans la liste
 */
const addTodoInList = () => {
    const todoElement = document.getElementById("todo-container")
    const newTodo = document.createElement("div")
    newTodo.classList.add("todo-element")
    todoElement.appendChild(newTodo)

    const vText = document.createElement("p")
    vText.classList.add("text-task")
    vText.textContent = inputEl.value
    newTodo.appendChild(vText)

    const checkBtn = document.createElement("button")
    checkBtn.classList.add("check-btn")
    checkBtn.innerHTML += `<i class="fa-solid fa-check"></i>`
    checkBtn.addEventListener('click', (e) => {
            console.log("check")
        });
    newTodo.appendChild(checkBtn)

    const editBtn = document.createElement("button")
    editBtn.classList.add("edit-btn")
    editBtn.innerHTML += `<i class="fa-solid fa-pen-to-square"></i>`
    editBtn.addEventListener('click', (e) => {
        console.log("edit")
    });
    newTodo.appendChild(editBtn)

    const trashBtn = document.createElement("button")
    trashBtn.classList.add("delete-btn")
    trashBtn.innerHTML = `<i class="fa-solid fa-trash"></i>`
    trashBtn.addEventListener('click', (e) => {
        e.target.parentElement.remove();
    });
    newTodo.appendChild(trashBtn)
    inputEl.value = null;
};


submitEl.addEventListener("click", async (e) => {
    e.preventDefault();
    await audioAlert(inputEl.value, addTodoInList);
});